��    8      �  O   �      �  (   �                 $   *     O     f     k       	   �     �     �     �     �     �     �  )   �       O   #     s     �     �     �     �  
   �     �     �     �     �  1   �     .  $   K     p  1   �     �     �     �     �             (         I  &   ]     �     �     �     �     �  
   �     �  	   �     �     �     �     �  �  	  7   �
     �
  
   �
  %   �
  6         W     r     �     �     �     �  #   �                    )  5   @  
   v  {   �  8   �  #   6     Z     h     �     �     �     �     �     �  A   �  &   1  7   X  D   �  H   �          -     A     W     s      �  B   �     �  \   �  
   \     g     x       0   �     �     �     �       
     
   *     5             &   1   -                         (          3   '          6                       5   *                           !       	       2      )   /   "   #       +                   $      .   8               
         7             4               0   ,                  %       
Found %d possible %s (%d %s scanned).

 Action Taken Analysis Check for updates Check or recheck a file's reputation ClamAV Signatures: %d
 Date Delete file results Directories Scanned:
 Directory Environment settings Error opening file Exit File File Analysis File has been saved File successfully submitted for analysis. Never No information exists for this file. Press OK to submit this file for analysis. No information on this file No threats found.
 None Please wait... Problems opening %s... Quarantine Restore Result Results Save results Scan all files and directories within a directory Scan directories recursively Scan files beginning with a dot (.*) Scan files larger than 20 MB Scan large files which are typically not examined Scanning %s... Select a file Set manually Signatures are current Status Submit file for analysis The antivirus signatures are out-of-date Unable to save file Unable to submit file: try again later Unknown Vendor View View file results View or delete previous results Viewing %s Virus Scanner Whitelist file files threat threats Project-Id-Version: clamtk
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-03-06 06:26-0600
PO-Revision-Date: 2014-05-23 20:55+0000
Last-Translator: nofallyaqoo <Unknown>
Language-Team: Arabic <ar@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2014-05-24 11:28+0000
X-Generator: Launchpad (build 17017)
 
عُثر على %d محتمل %s (%d %s مفحوص).

 الإجراء المتخذ تحليل تأكد من وجود تحديثات تحقق من سمعة الملف أو قم بفحصة تواقيع ClamAV: %d\n
 التاريخ حذف نتائج الملف الأدلة المفحوصة:
 دليل إعدادات البيئة خطأ أثناء فتح الملف خروج ملف تحليل ملف تم حفظ الملف تم تقديم الملف للتحليل بنجاح. أبدًا لا توجد معلومات لهذا الملف. اضغط على موافق لتقديم هذا الملف للتحليل. لا يوجد أي معلومات عن هذا الملف لم توجد أي تهديدات.
 لا يوجد إنتظر من فضلك... مشكلات في فتح %s... إحجر استعادة النتيجة النتائج حفظ النتائج فحص جميع الملفات والأدلة ضمن الدليل مسح المسارات مُكرراً فحص الملفات التي تبدء بنقطة (.*) إحذف الملفات التي تكبر عن ٢٠ ميغابايت مسح الملفات الكبيرة التي عادةً لا تُفحص يفحص %s... اختيار ملف إضبط يدوياً التواقيع حديثة الحالة تقديم ملف للتحليل تواقيع لِمُضاد الفيروسات غير مُحدثة تعذر حفظ الملف ير قادر على تحليل الملف:  حاولة مرة أخرى في وقت لاحق مجهول المُنتِج عرض عرض نتائج الملف عرض أو حذف النتائج السابقة يتم عرض %s ماسح للفيروسات القائمة البيضاء ملف ملفات تهديد تهديدات 